# Cardiff Court

Images, sounds and playful [Acro Avatars](https://gitlab.com/their-temple/acro-avatars) from [Their Temple](http://theirtemple.com) at Cardiff Court

Located [here](http://theirtemple.com/cardiff)

## Features

[Acro Avatars](https://gitlab.com/their-temple/acro-avatars) 
- People
  - Custodian
  - [Hanumanji](https://gitlab.com/Hanumanji)
- Places
  - Courtyard Gates
  - Courtyard
  - House of Play
  - 209
    - Inviting Space
    - Inviting Closet
    - Inviting Bath
    - Aspiring Space 
    - Aspiring Closet
    - Cherishing Space
    - Cherishing Closet
    - Cherishing Bath
    - Nourishing Space 
    - Divining Space 
  - Elevator
  - Garage
    - #? (Space for 209)
  - Garage Gate
- Things
  - [Scout](https://gitlab.com/their-temple/scout)

## Demo

## Installation

## License
